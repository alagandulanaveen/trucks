from django.db import models


class Truck(models.Model):
    number_plate=models.CharField(max_length=100)
    is_signed=models.BooleanField(default=False)
    signed_to=models.CharField(max_length=100,blank=True)


