from django.urls import path
from . import views

urlpatterns = [
    path('trucks/', views.trucks, name='trucks'),
    path('sign/<int:truck_id>/', views.sign_truck, name='sign_truck'),
    path('signed/', views.signed_trucks, name='signed_trucks'),
    path('complete/<int:truck_id>/', views.complete_task, name='complete'),
]