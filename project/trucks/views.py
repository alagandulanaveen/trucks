from django.shortcuts import render, redirect
from .models import Truck


def trucks(request):
    trucks = Truck.objects.filter(is_signed=False)
    return render(request, 'trucks/trucks.html', {'trucks': trucks})

def sign_truck(request, truck_id):
    truck = Truck.objects.get(id=truck_id)
    truck.is_signed = True
    truck.signed_to = 'Deliver Goods'  # Replace with the actual task name
    truck.save()
    return redirect('trucks')

def signed_trucks(request):
    trucks = Truck.objects.filter(is_signed=True)
    return render(request, 'trucks/signed_trucks.html', {'trucks': trucks})

def complete_task(request, truck_id):
    truck = Truck.objects.get(id=truck_id)
    truck.is_signed = False
    truck.signed_to = ''
    truck.save()
    return redirect('signed_trucks')
